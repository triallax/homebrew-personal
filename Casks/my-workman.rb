# Upstream Workman macOS bundle has an unfixed issue that causes Ctrl+key
# to not work properly. See
# https://codeberg.org/triallax/Workman/commit/fb4474dba602c54e8d6c869c8a6a80a1c67001c7.
cask "my-workman" do
  version :latest
  sha256 :no_check

  url "https://codeberg.org/triallax/Workman/archive/fix-ctrl-key.tar.gz"
  name "Workman keyboard layout"
  desc "Alternative English keyboard layout"
  homepage "https://workmanlayout.org/"

  artifact "workman/mac/Workman.bundle", target: "#{Dir.home}/Library/Keyboard Layouts/Workman.bundle"

  caveats do
    logout
  end
end
