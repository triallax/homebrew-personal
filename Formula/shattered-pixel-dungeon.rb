class ShatteredPixelDungeon < Formula
  desc "Traditional roguelike game with pixel-art graphics and simple interface"
  homepage "https://shatteredpixel.com/shatteredpd/"
  url "https://github.com/00-Evan/shattered-pixel-dungeon/archive/refs/tags/v1.4.1.tar.gz"
  sha256 "eceada45771db6355b865f8247b9ca92137bb9f055131d8d81a7f25e5a1fff36"
  license "GPL-3.0-or-later"

  livecheck do
    url :stable
    # Ignore beta tags.
    regex(/^v?(\d+(?:\.\d+)+)$/i)
  end

  depends_on "openjdk@11"

  def install
    ENV["JAVA_HOME"] = Formula["openjdk@11"].opt_prefix
    system "./gradlew", "desktop:release"
    # Stop the daemon so it doesn't hang around hogging memory and other
    # resources.
    system "./gradlew", "--stop"
    libexec.install "desktop/build/libs/desktop-#{version}.jar" => "spd.jar"
    bin.write_jar_script libexec/"spd.jar", "spd", "-XstartOnFirstThread"
  end
end
