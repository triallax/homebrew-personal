class Spt < Formula
  desc "Simple pomodoro timer that doubles your efficiency"
  homepage "https://github.com/pickfire/spt"
  license "ISC"
  head "https://github.com/pickfire/spt.git"

  depends_on "terminal-notifier"

  patch :DATA

  def install
    system "make", "install", "PREFIX=#{prefix}"
  end
end

__END__
diff --git a/Makefile b/Makefile
index 05e4894..769ba67 100644
--- a/Makefile
+++ b/Makefile
@@ -27,6 +27,10 @@ config.h:
 spt: ${OBJ}
 	@echo CC -o $@
 	@${CC} -o $@ ${OBJ} ${LDFLAGS}
+	@# Reduce binary size a little (I know, why optimize an already tiny
+	@# binary? It's stupid.).
+	@strip -S -N -x spt
+
 
 clean:
 	@echo cleaning
diff --git a/config.mk b/config.mk
index 8c9d6d5..3e80779 100644
--- a/config.mk
+++ b/config.mk
@@ -11,14 +11,14 @@ INCS = -I. -I/usr/include
 LIBS = -L/usr/lib
 
 # libnotify, comment if you don't want it
-DEFS = -DNOTIFY
-INCS+= `pkg-config --cflags libnotify`
-LIBS+= `pkg-config --libs libnotify`
+#DEFS = -DNOTIFY
+#INCS+= `pkg-config --cflags libnotify`
+#LIBS+= `pkg-config --libs libnotify`
 
 # flags
 CPPFLAGS += -DVERSION=\"${VERSION}\" -D_POSIX_C_SOURCE=199309
-CFLAGS += -g -std=c99 -pedantic -Wall -Os ${INCS} ${DEFS} ${CPPFLAGS}
-LDFLAGS += -g ${LIBS}
+CFLAGS += -std=c99 -pedantic -Wall -Os ${INCS} ${DEFS} ${CPPFLAGS}
+LDFLAGS += ${LIBS}
 
 # compiler and linker
 CC ?= cc
diff --git a/spt.c b/spt.c
index aec2f42..5ba8931 100644
--- a/spt.c
+++ b/spt.c
@@ -63,8 +63,7 @@ spawn(char *argv[])
 void
 notify_send(char *cmt)
 {
-	if (strcmp(notifycmd, ""))
-		spawn((char *[]) { notifycmd, "spt", cmt, NULL });
+	spawn((char *[]) { "terminal-notifier", "-title", "spt", "-message", cmt, "-sound", "default", NULL });
 #ifdef NOTIFY
 	else {
 		notify_init("spt");
