# v0.2.4 currently doesn't work properly with tmux
# (https://github.com/tulir/gomuks/issues/250), but this has since been fixed,
# though the fix hasn't been released yet. When a release with the fix is made,
# I'll submit the formula to homebrew-core.
class Gomuks < Formula
  desc "Terminal-based Matrix client written in Go"
  homepage "https://maunium.net/go/gomuks"
  license "AGPL-3.0-or-later"
  head "https://github.com/tulir/gomuks.git", branch: "master"

  depends_on "go" => :build
  depends_on "libolm"

  def install
    system "go", "build", *std_go_args(ldflags: "-s -w")
  end
end
